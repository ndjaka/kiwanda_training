import React, {Component} from 'react';
import Day from "./components/Day";
import DayStatus from "./components/DayStatus";
import Time from "./components/Time";
import ClockDate from "./components/ClockDate";
import UpdateClock from "./components/UpdateClock";
import moment from "moment";

class Clock extends Component {

    constructor(props) {
        super(props);

        this.state = {
            date: new Date(),
            year: new Date().getFullYear(),
            day: new Date().getDate(),
            months: new Date().getMonth(),
            update: false,
            hours:new Date().getHours(),
            min:new Date().getMinutes()
        };
    }

    componentDidMount() {
        this.startTimer()
    }

    componentWillUnmount() {
        this.stopTimer()
    }


    getPeriod = (hour) => {
        const _hour = parseInt(hour)
        if (_hour > 0 && _hour < 6 ) {
            return 'Nuit'
        }
        if (_hour > 6 && _hour < 12) {
            return 'Matinée'
        }

        if (_hour > 12 && _hour < 18) {
            return 'Après Midi'
        }

        if (_hour >= 18) {
            return 'Soir'
        }
    }

    startTimer = () => {
        this.timerID = setInterval(
            () => {
                let _date = new Date();

                _date.setUTCFullYear(this.state.year,
                    this.state.months,
                    this.state.day);
                _date.setHours(this.state.hours,this.state.min)
                this.setState(prevState => ({
                    ...prevState,
                    date: _date,
                }))
            },
            1000
        );
    }

    stopTimer() {
        clearInterval(this.timerID);
    }

    handleChangeDate(state) {
        this.stopTimer()
        this.setState(prevState => ({
            ...prevState,
            date: new Date(state),
            year: new Date(state).getFullYear(),
            day: new Date(state).getDate(),
            months: new Date(state).getMonth(),
            hours: new Date(state).getHours(),
            min: new Date(state).getMinutes(),
            update: false
        }));
        this.startTimer()


    }

    render() {
        return (
            <div className={'clock-container'}>
                <div className={'clock-wrapper'}>
                    <div className={'clock-content'}>
                        {!this.state.update ? <div className={'clock-display'}>
                                <Day day={this.state.date}/>
                                <DayStatus daystatus={this.getPeriod(this.state.date.getHours())}/>
                                <Time time={this.state.date}/>
                                <ClockDate date={this.state.date}/>
                            </div> :
                            <UpdateClock setDate={(state) => {
                                if(state !== '')
                                this.handleChangeDate(state)
                            }}/>}
                    </div>
                    {!this.state.update && <div className={'clock-btn-container'}>
                        <input type={'button'} value={'Modifier'} className={'clock-btn'}
                               onClick={() => {
                                   this.setState(prevState => ({
                                       ...prevState,
                                       update: true
                                   }))
                               }}/>
                    </div>}
                </div>
            </div>
        );
    }
}

export default Clock;