import React from 'react';
import moment from "moment";

function ClockDate({date}) {

    return (
        <div className={'clock-date'}>{moment(date).format('DD  MMMM YYYY')}</div>

    );
}


export default ClockDate;