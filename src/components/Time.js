import React from 'react';
import moment from "moment";

function Time({time}) {

    return (
        <div className={'clock-time'}>{moment(time).format('HH:mm:ss')}</div>

    );
}


export default Time;