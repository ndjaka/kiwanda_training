import React, {useState} from 'react';
import TimePicker from 'react-time-picker';
import DatePicker from 'react-date-picker';
import 'react-calendar/dist/Calendar.css'
import 'react-time-picker/dist/TimePicker.css'
import moment from "moment";

function UpdateClock({setDate}) {
    const [value, onChange] = useState(new Date());
    const [hour, onChangeHour] = useState(moment(value).format('h:mm'));

    return (
        <div>
            <div className={'date-wrapper'}>
                <div>
                    <label>Date</label>
                    <DatePicker
                        className={'date-field'}
                        onChange={onChange}
                        value={value}
                    />
                </div>
                <div>
                    <label>Heure</label>
                    <TimePicker
                        className={'date-field'}
                        disableClock
                        onChange={onChangeHour}
                        value={hour}
                    />
                </div>
            </div>
            <div className={'clock-btn-container'}>
                <input type={'button'} value={'Enregistrer'} className={'clock-btn'}
                       onClick={() => {

                           setDate(`${moment(value).format('YYYY-MM-DD')} ${hour}`)
                       }

                       }/>
            </div>
        </div>
    );
}


export default UpdateClock;