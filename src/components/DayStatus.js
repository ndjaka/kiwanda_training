import React from 'react';

function DayStatus({daystatus}) {

    return (
        <div className={'clock-time-status'}>{daystatus}</div>

    );
}


export default DayStatus;