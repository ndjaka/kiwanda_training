import React from 'react';
import moment from "moment";


function Day({day}) {

    return (
        <div className={'clock-day'}>{moment(day).format('dddd')}</div>

    );
}


export default Day;